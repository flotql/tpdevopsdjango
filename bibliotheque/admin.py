from re import A
from django.contrib import admin
from .models import Livre, Categorie, Auteur, Langue

admin.site.register(Livre)
admin.site.register(Categorie)
admin.site.register(Langue)
admin.site.register(Auteur)


