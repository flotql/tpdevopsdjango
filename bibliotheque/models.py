from unicodedata import category
from django.db import models
import datetime
from django.contrib.auth.models import User 


class Categorie(models.Model):
    nom_categorie = models.CharField(max_length=100)

    def __str__(self):
        return self.nom_categorie

class Langue(models.Model):
    langue = models.CharField(max_length=50)

    def __str__(self):
        return self.langue

class Auteur(models.Model):
    nom_auteur = models.CharField(max_length=100)

    def __str__(self):
        return self.nom_auteur

class Livre(models.Model):
    nom_livre = models.CharField(max_length=100)
    categorie = models.ForeignKey(Categorie, on_delete=models.CASCADE, blank=True, null=True)
    langue = models.ForeignKey(Langue, on_delete=models.CASCADE, blank=True, null=True)
    auteur = models.ForeignKey(Auteur, on_delete=models.CASCADE, blank=True, null=True)
 
    def __str__(self):
       return f'{self.nom_livre} {self.categorie} {self.langue} {self.auteur}'
       

