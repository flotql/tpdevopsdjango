from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('livres', views.all_livre, name='livres'),
    path('auteurs', views.auteur, name='auteurs'),
    path('categories', views.categorie, name='categories'),
    path('langues', views.langue, name='langues'),


]