from django.shortcuts import render
from .models import Livre

def index(request):
    return render(request, 'bibliotheque/index.html' )


def all_livre(request):
    list_livres = Livre.objects.all()
    return render(request, 'biliotheque/livres.html', {'list_livres' : list_livres})

def auteur(request):
    return render(request, 'biliotheque/auteurs.html')

def categorie(request):
    return render(request, 'biliotheque/categories.html')
    
def langue(request):
    return render(request, 'biliotheque/langues.html')        